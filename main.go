package main

import (
	"fmt"
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	_ "g06-order-service/component/traceprovider"
	tp "g06-order-service/component/traceprovider/otel"
	"g06-order-service/kafka"
	"g06-order-service/kafka/kafkapb"
	"g06-order-service/memcache"
	"g06-order-service/middleware"
	ginorder "g06-order-service/modules/order/transport/gin"
	userstorage "g06-order-service/modules/user/storage"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/onsi/ginkgo/reporters/stenographer/support/go-colorable"
	log "github.com/sirupsen/logrus"
	"github.com/uptrace/opentelemetry-go-extra/otelgorm"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
)

func main() {

	formatter := new(log.TextFormatter)
	formatter.ForceColors = true
	formatter.TimestampFormat = "2006-01-02 15:04:05"
	formatter.FullTimestamp = true
	log.SetFormatter(formatter)
	log.SetOutput(colorable.NewColorableStdout())
	// Config Log
	if os.Getenv("GIN_MODE") == "RELEASE" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.TraceLevel)
	}

	log.Info("Setup Config Log")
	// End Config Log

	err := godotenv.Load()
	if err != nil {
		log.Info("Error loading .env file")
	}

	dbHost := os.Getenv("DB_HOST")
	dbDatabase := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	secretKey := os.Getenv("SYSTEM_SECRET")
	ginPort := os.Getenv("GIN_PORT")
	jaegerService := os.Getenv("JAEGER_SERVICE")
	serviceName := os.Getenv("SERVICE_NAME")
	kafkaService := os.Getenv("KAFKA_SERVICE")
	mysqlConnection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true&loc=Local", dbUser, dbPassword, dbHost, dbPort, dbDatabase)

	db, err := gorm.Open(mysql.Open(mysqlConnection), &gorm.Config{})
	log.Info("Connect Mysql")
	if err != nil {
		log.Fatalln("Error mysql:", err)
		panic(err)
	}

	if err := db.Use(otelgorm.NewPlugin()); err != nil {
		panic(err)
	}
	db = db.Debug()

	log.Info("Setup Gin Router")
	r := gin.Default()

	service := map[string]string{
		"AUTH_SERVICE":       os.Getenv("AUTH_SERVICE"),
		"RESTAURANT_SERVICE": os.Getenv("RESTAURANT_SERVICE"),
		"UPLOAD_SERVICE":     os.Getenv("UPLOAD_SERVICE"),
	}
	client := kafkapb.NewKafkaPubSub(kafkaService)
	listService := *common.NewService(service)
	trace := tp.NewTraceProvider(serviceName, jaegerService)

	appCtx := appctx.NewAppContext(db, secretKey, client, trace, listService)

	kafka.NewSubscriber(appCtx).Start()

	r.Use(middleware.Recover(appCtx))
	r.Use(otelgin.Middleware(serviceName))

	authStore := userstorage.NewSQLStore(appCtx.GetMainDBConnection())
	userCaching := memcache.NewUserCaching(memcache.NewCaching(), authStore)
	middlewareAuth := middleware.RequireAuth(appCtx, userCaching)
	v1 := r.Group("/v1", middlewareAuth)
	{
		// Orders
		v1.POST("", ginorder.CreateOrder(appCtx))

		v1.GET("", ginorder.ListOrder(appCtx))

		v1.GET("/:id", ginorder.GetOrder(appCtx))

		v1.PUT("/:id", ginorder.UpdateOrder(appCtx))

		v1.DELETE("/:id", ginorder.DeleteOrder(appCtx))
	}

	trace.SetProvider()
	err = r.Run(ginPort)
	if err != nil {
		log.Fatalln(err)
	} // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

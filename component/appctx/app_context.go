package appctx

import (
	"g06-order-service/common"
	"g06-order-service/component/traceprovider"
	"g06-order-service/kafka"
	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
	GetKafka() kafka.PubSub
	TraceProvider() traceprovider.Provider
	ListService() common.ListService
	GetService(key string) string
}

type appCtx struct {
	db            *gorm.DB
	secretKey     string
	kafkaPs       kafka.PubSub
	traceProvider traceprovider.Provider
	listService   common.ListService
}

func NewAppContext(db *gorm.DB, secretKey string, kafkaPs kafka.PubSub, traceProvider traceprovider.Provider, listService common.ListService) *appCtx {
	return &appCtx{db: db, secretKey: secretKey, kafkaPs: kafkaPs, traceProvider: traceProvider, listService: listService}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB { return ctx.db }

func (ctx *appCtx) SecretKey() string { return ctx.secretKey }

func (ctx *appCtx) GetKafka() kafka.PubSub { return ctx.kafkaPs }

func (ctx *appCtx) TraceProvider() traceprovider.Provider { return ctx.traceProvider }

func (ctx *appCtx) ListService() common.ListService { return ctx.listService }

func (ctx *appCtx) GetService(key string) string { return ctx.listService.GetService(key) }

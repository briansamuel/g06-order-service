package middleware

import (
	"context"
	"errors"
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	"g06-order-service/component/tokenprovider/jwt"
	usermodel "g06-order-service/modules/user/model"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/attribute"
	"strings"
)

type AuthenStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
}

func ErrWrongAuthHeader(err error) *common.AppError {
	return common.NewCustomError(
		errors.New("error wrong authen header"),
		"error wrong authen header",
		"ErrWrongAuthHeader",
	)
}

func extractTokenFromHeaderString(s string) (string, error) {
	parts := strings.Split(s, " ")

	if parts[0] != "Bearer" || len(parts) < 2 || strings.TrimSpace(parts[1]) == "" {
		return "", ErrWrongAuthHeader(nil)
	}
	return parts[1], nil
}

// 1. Get token from header
// 2. Validate Token and parse to payload
// 3. From the token Payload, we use user_id to find from DB
func RequireAuth(appCtx appctx.AppContext, authStore AuthenStore) func(c *gin.Context) {
	tokenProvider := jwt.NewTokenJWTProvider(appCtx.SecretKey())

	return func(c *gin.Context) {

		tr := appCtx.TraceProvider().GetTracer()
		_, span := tr.Start(c.Request.Context(), "Authentication")

		token, err := extractTokenFromHeaderString(c.GetHeader("Authorization"))
		if err != nil {

			log.WithField("middleware", "extractTokenFromHeaderString").Info(err)
			span.End()
			panic(err)
		}

		payload, err := tokenProvider.Validate(token)
		if err != nil {

			log.WithField("middleware", "tokenProvider Validate").Info(err)
			span.End()
			panic(err)
		}

		service := appCtx.GetService("AUTH_SERVICE")
		user, err := authStore.FindUser(c.Request.Context(), map[string]interface{}{"id": payload.UserId, "service": service, "token": token})
		if err != nil {

			log.WithField("middleware", "authStore FindUser").Info(err)
			span.End()
			panic(err)
		}

		if user.Status == 0 {

			panic(common.ErrNoPermission(errors.New("user has been deleted or banner")))
		}
		user.Token = token
		user.Unmask()
		span.SetAttributes(attribute.Key("user_id").Int(user.ID))
		span.End()
		c.Set(common.CurrentUser, user)
		c.Next()
	}
}

package common

import "fmt"

func Recover() {
	if r := recover(); r != nil {
		fmt.Println("WOHA! Program is panicking with value", r)
	}
}

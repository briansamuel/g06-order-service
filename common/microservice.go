package common

type ListService struct {
	listService map[string]string
}

func NewService(listService map[string]string) *ListService {
	return &ListService{listService: listService}
}

func (svs *ListService) GetService(key string) string {
	service := svs.listService[key]
	return service
}

func (svs *ListService) AddService(key, service string) {

	svs.listService[key] = service
}

package ginorder

import (
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	orderbusiness "g06-order-service/modules/order/business"
	orderstorage "g06-order-service/modules/order/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func DeleteOrder(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInternal(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)
		store := orderstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := orderbusiness.NewDeleteOrderBiz(store)

		if err := biz.DeleteOrder(c.Request.Context(), int(id.GetLocalID()), requester.GetUserId(), true); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(id))
	}
}

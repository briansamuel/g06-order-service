package ginorder

import (
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	orderbusiness "g06-order-service/modules/order/business"
	ordermodel "g06-order-service/modules/order/model"
	orderstorage "g06-order-service/modules/order/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateOrder(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var newOrder ordermodel.OrderCreate

		if err := c.ShouldBind(&newOrder); err != nil {
			panic(common.ErrInternal(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)
		newOrder.UserId = requester.GetUserId()
		store := orderstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := orderbusiness.NewCreateFoodBiz(store)
		if err := biz.CreateOrder(c.Request.Context(), &newOrder); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(newOrder.ID))
	}
}

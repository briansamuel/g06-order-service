package ginorder

import (
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	orderbusiness "g06-order-service/modules/order/business"
	orderstorage "g06-order-service/modules/order/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetOrder(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		store := orderstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := orderbusiness.NewGetOrderBiz(store)

		data, err := biz.GetOrder(c.Request.Context(), int(id.GetLocalID()))
		if err != nil {
			panic(err)
		}
		data.Mask(false)
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}

package ginorder

import (
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	orderbusiness "g06-order-service/modules/order/business"
	ordermodel "g06-order-service/modules/order/model"
	orderstorage "g06-order-service/modules/order/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UpdateOrder(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInternal(err))
		}
		var data ordermodel.OrderUpdate
		if err := c.ShouldBind(&data); err != nil {
			panic(common.ErrInternal(err))
		}

		store := orderstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := orderbusiness.NewUpdateOrderBiz(store)

		if err := biz.UpdateOrder(c.Request.Context(), int(id.GetLocalID()), &data); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}

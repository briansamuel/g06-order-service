package ginorder

import (
	"g06-order-service/common"
	"g06-order-service/component/appctx"
	orderbusiness "g06-order-service/modules/order/business"
	ordermodel "g06-order-service/modules/order/model"
	orderstorage "g06-order-service/modules/order/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func ListOrder(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter ordermodel.Filter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}

		// if err := db.Table(Restaurant{}.TableName()).Count(&paging.Total).Error; err != nil {
		// 	c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		// 	return
		// }
		requester := c.MustGet(common.CurrentUser).(common.Requester)
		filter.UserId = requester.GetUserId()

		store := orderstorage.NewSQLStore(appContext.GetMainDBConnection())

		biz := orderbusiness.NewListRestaurantBiz(store)

		result, err := biz.ListOrder(c.Request.Context(), &filter, &paging)
		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}

package ordermodel

import "g06-order-service/common"

type OrderCreate struct {
	common.SQLModel
	TotalPrice int `json:"total_price" gorm:"total_price"`
	ShipperId  int `json:"-" gorm:"shipper_id"`
	UserId     int `json:"-" gorm:"user_id"`
}

func (OrderCreate) TableName() string { return Order{}.TableName() }

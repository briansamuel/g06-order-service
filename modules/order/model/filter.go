package ordermodel

type Filter struct {
	TotalPrice int `json:"total_price" gorm:"total_price"`
	ShipperId  int `json:"-" gorm:"shipper_id"`
	UserId     int `json:"-" gorm:"user_id"`
}

func (Filter) TableName() string { return Order{}.TableName() }

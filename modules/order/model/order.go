package ordermodel

import (
	"g06-order-service/common"
)

const EntityName = "Order"

type Order struct {
	common.SQLModel
	TotalPrice int   `json:"total_price" gorm:"total_price"`
	ShipperId  int   `json:"-" gorm:"shipper_id"`
	UserId     int   `json:"-" gorm:"user_id"`
	User       *User `json:"user" gorm:"preload:false;"`
	//Shipper    *User `json:"shipper" gorm:"foreignKey:ShipperId;preload:false;"`
}

func (Order) TableName() string { return "orders" }

func (data *Order) Mask(isOwnerOrAdmin bool) {
	data.GenUID(common.DbTypeOrder)

	if u := data.User; u != nil {
		u.Mask(isOwnerOrAdmin)
	}

	//if s := data.Shipper; s != nil {
	//	s.Mask(isOwnerOrAdmin)
	//}
}

type User struct {
	ID        int           `json:"-" gorm:"id"`
	FakeId    common.UID    `json:"id" gorm:"-"`
	LastName  string        `json:"last_name" gorm:"column:last_name;"`
	FirstName string        `json:"first_name" gorm:"column:first_name;"`
	Avatar    *common.Image `json:"avatar" gorm:"avatar"`
}

func (User) TableName() string { return "users" }

func (u *User) Mask(isAdmin bool) {
	u.FakeId = common.NewUID(uint32(u.ID), common.DbTypeUser, 1)
}

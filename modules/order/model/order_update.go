package ordermodel

type OrderUpdate struct {
	TotalPrice int  `json:"total_price" gorm:"total_price"`
	ShipperId  int  `json:"-" gorm:"shipper_id"`
	UserId     int  `json:"" gorm:"user_id"`
	Status     *int `json:"status" gorm:"column:status;default:1;"`
}

func (OrderUpdate) TableName() string { return Order{}.TableName() }

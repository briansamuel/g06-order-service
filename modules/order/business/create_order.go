package orderbusiness

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

type CreateOrderStorage interface {
	Create(ctx context.Context, data *ordermodel.OrderCreate) error
}

type createOrderBiz struct {
	store CreateOrderStorage
}

func NewCreateFoodBiz(store CreateOrderStorage) *createOrderBiz {
	return &createOrderBiz{store: store}
}

func (biz *createOrderBiz) CreateOrder(ctx context.Context, data *ordermodel.OrderCreate) error {

	err := biz.store.Create(ctx, data)
	if err != nil {
		return common.ErrCannotCreateEntity(ordermodel.EntityName, err)
	}

	return nil
}

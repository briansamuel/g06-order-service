package orderbusiness

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

type UpdateOrderStorage interface {
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*ordermodel.Order, error)
	Update(ctx context.Context, id int, data *ordermodel.OrderUpdate) error
}

type updateOrderBiz struct {
	store UpdateOrderStorage
}

func NewUpdateOrderBiz(store UpdateOrderStorage) *updateOrderBiz {
	return &updateOrderBiz{
		store: store,
	}
}

func (biz *updateOrderBiz) UpdateOrder(ctx context.Context, id int, data *ordermodel.OrderUpdate) error {

	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return common.ErrCannotUpdateEntity(ordermodel.EntityName, err)
	}
	if oldData.Status == 0 {
		return common.ErrEntityNotFound(ordermodel.EntityName, err)
	}

	if err := biz.store.Update(ctx, id, data); err != nil {

		return common.ErrCannotUpdateEntity(ordermodel.EntityName, err)
	}
	return nil
}

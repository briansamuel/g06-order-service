package orderbusiness

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

type ListOrderStore interface {
	ListDataWithCondition(
		ctx context.Context,
		filter *ordermodel.Filter,
		paging *common.Paging) ([]ordermodel.Order, error)
}

type listOrderBiz struct {
	store ListOrderStore
}

func NewListRestaurantBiz(store ListOrderStore) *listOrderBiz {
	return &listOrderBiz{store: store}
}

func (biz *listOrderBiz) ListOrder(
	ctx context.Context,
	filter *ordermodel.Filter,
	paging *common.Paging) ([]ordermodel.Order, error) {

	result, err := biz.store.ListDataWithCondition(ctx, filter, paging)
	if err != nil {
		return nil, common.ErrCannotListEntity(ordermodel.EntityName, err)
	}

	return result, nil
}

package orderbusiness

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

type GetOrderStore interface {
	GetDataWithCondition(
		ctx context.Context,
		cond map[string]interface{}) (*ordermodel.Order, error)
}

type getRestaurantBiz struct {
	store GetOrderStore
}

func NewGetOrderBiz(store GetOrderStore) *getRestaurantBiz {
	return &getRestaurantBiz{store: store}
}

func (biz *getRestaurantBiz) GetOrder(
	ctx context.Context,
	id int) (*ordermodel.Order, error) {

	data, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})
	if err != nil {
		if err == common.RecordNotFound {
			return nil, common.ErrEntityNotFound(ordermodel.EntityName, err)
		}
		return nil, common.ErrCannotGetEntity(ordermodel.EntityName, err)
	}
	if data.Status == 0 {
		return nil, common.ErrEntityDeleted(ordermodel.EntityName, err)
	}
	return data, nil
}

package orderbusiness

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

type DeleteOrderStorage interface {
	Delete(ctx context.Context, id int) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*ordermodel.Order, error)
	Update(ctx context.Context, id int, data *ordermodel.OrderUpdate) error
}

type deleteOrderBiz struct {
	store DeleteOrderStorage
}

func NewDeleteOrderBiz(store DeleteOrderStorage) *deleteOrderBiz {
	return &deleteOrderBiz{
		store: store,
	}
}

func (biz *deleteOrderBiz) DeleteOrder(ctx context.Context, id int, userId int, isSoft bool) error {
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})

	if err != nil {

		return err
	}
	if oldData.Status == 0 {
		return common.ErrEntityDeleted(ordermodel.EntityName, err)
	}

	if oldData.User.ID != userId {
		return common.ErrEntityNotFound(ordermodel.EntityName, err)
	}

	if isSoft {
		zero := 0
		if err := biz.store.Update(ctx, id, &ordermodel.OrderUpdate{Status: &zero}); err != nil {

			return common.ErrCannotDeleteEntity(ordermodel.EntityName, err)
		}
		return nil
	}

	if err := biz.store.Delete(ctx, id); err != nil {

		return err
	}
	return nil
}

package orderstorage

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
	"gorm.io/gorm"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*ordermodel.Order, error) {

	var data ordermodel.Order
	if err := s.db.
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}

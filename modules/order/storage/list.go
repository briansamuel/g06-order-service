package orderstorage

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

func (s *sqlStore) ListDataWithCondition(ctx context.Context,
	filter *ordermodel.Filter,
	paging *common.Paging) ([]ordermodel.Order, error) {
	db := s.db
	var result []ordermodel.Order

	db = db.Where("status in (?)", 1)

	if filter.ShipperId > 0 {
		db = db.Where("shipper_id = ?", filter.ShipperId)
	}

	if filter.UserId > 0 {
		db = db.Where("user_id = ?", filter.UserId)
	}

	if err := db.Table(ordermodel.Order{}.TableName()).Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	db = db.Preload("User")
	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("id desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}

	return result, nil
}

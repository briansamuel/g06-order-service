package orderstorage

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *ordermodel.OrderUpdate) error {
	db := s.db

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

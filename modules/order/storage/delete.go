package orderstorage

import (
	"context"
	"g06-order-service/common"
	ordermodel "g06-order-service/modules/order/model"
)

func (s *sqlStore) Delete(ctx context.Context, id int) error {
	db := s.db

	if err := db.Table(ordermodel.Order{}.TableName()).Where("id = ?", id).Delete(nil).Error; err != nil {

		return common.ErrDB(err)
	}

	return nil
}
